import computedMath from "../common/computedMath.js";

const add = computedMath((a, b) => a + b, 0)
export default add;