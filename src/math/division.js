import computedMath from "../common/computedMath.js";

const division =  computedMath((a, b) => a / b, 0);

export default division;