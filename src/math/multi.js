import computedMath from "../common/computedMath.js";

const multi = computedMath((a, b) => a * b, 0);

export default multi;