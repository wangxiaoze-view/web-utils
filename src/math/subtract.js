import computedMath from "../common/computedMath.js";

const subtract = computedMath((a, b) => a - b, 0);

export default subtract;